const http = require("http");

const port = 4000;

const server = http.createServer(function (req, res){

	if(req.url =="/" && req.method == "GET") {
        res.writeHead(200, {'Content-Type': 'application/json'});
		res.end("Welcome to the booking system");

	}else if(req.url =="/profile" && req.method == "GET"){
        res.writeHead(200, {'Content-Type': 'application/json'});
		res.end("Welcome to your profile!");

    }else if(req.url =="/courses" && req.method == "GET"){
        res.writeHead(200, {'Content-Type': 'application/json'});
		res.end("Here's our courses available");
    }
    
    if(req.url =="/addcourse" && req.method == "POST"){
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end("Add a course to our resources");
    }

    if(req.url =="/updatecourse" && req.method == "PUT") {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end("Update course to our resources");
    }

    if(req.url =="/archivecourses" && req.method == "DELETE") {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end("Archive courses to our resources");
    }
    
})


server.listen(port);

console.log(`Server is running at localhost:${port}`);